//
//  ViewController.swift
//  CustomViews
//
//  Created by Bilal Mustafa on 1/26/18.
//  Copyright © 2018 Bilal Mustafa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var imageview: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        var frame = redView.frame
      //  let scrollView = UIScrollView(frame: frame)
      //  scrollView.backgroundColor = UIColor.blue
      //  scrollView.showsVerticalScrollIndicator = true
      //  scrollView.showsHorizontalScrollIndicator  = true
      //  self.redView.addSubview(scrollView)
        
       // view.backgroundColor = UIColor.white
        // the image we're going to mask and shadow
        let image = UIImageView(image: UIImage(named: "images"))
        image.center = redView.center
        // make new layer to contain shadow and masked image
        let containerLayer = CALayer()
        containerLayer.shadowColor = UIColor.black.cgColor
        containerLayer.shadowRadius = 10.0
        containerLayer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        containerLayer.shadowOpacity = 1.0
        // use the image's layer to mask the image into a circle
        image.layer.cornerRadius = CGFloat(roundf(Float(image.frame.size.width / 2.0)))
        image.layer.masksToBounds = true
        // add masked image layer into container layer so that it's shadowed
        containerLayer.addSublayer(image.layer)
        // add container including masked image and shadow into view
        self.redView.layer.addSublayer(containerLayer)
 
        
        
        drawTriangle(size: 200, x: self.redView.frame.origin.x + 170, y: self.redView.frame.origin.y + 10, up: true)

    }
    
    
    
    
    func drawTriangle(size: CGFloat, x: CGFloat, y: CGFloat, up:Bool) {
        
        let triangleLayer = CAShapeLayer()
        let trianglePath = UIBezierPath()
        trianglePath.move(to: .zero)
        trianglePath.addLine(to: CGPoint(x: -size, y: up ? size : -size))
        trianglePath.addLine(to: CGPoint(x: size, y: up ? size : -size))
        trianglePath.close()
        triangleLayer.path = trianglePath.cgPath
        triangleLayer.fillColor = UIColor.white.cgColor
        triangleLayer.strokeColor = UIColor.black.cgColor
        triangleLayer.fillRule = kCAFillRuleEvenOdd
        triangleLayer.anchorPoint = .zero
        triangleLayer.position = CGPoint(x: x, y: y)
        triangleLayer.name = "triangle"
        //triangleLayer.masksToBounds = true
        //view.clipsToBounds = true
        //redView.layer.addSublayer(triangleLayer)
        //self.redView.layer.mask = triangleLayer
        
        let containerLayer = CALayer()
        containerLayer.shadowColor = UIColor.black.cgColor
        containerLayer.shadowRadius = 10.0
        containerLayer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        containerLayer.shadowOpacity = 1.0
       
        containerLayer.addSublayer(triangleLayer)
        self.redView.layer.addSublayer(containerLayer)
        
       self.redView.layer.mask = containerLayer
       
        //self.redView.layer.mask?.borderColor = UIColor.purple.cgColor
        //self.redView.layer.mask?.borderWidth = 10.0
 
        
       
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

